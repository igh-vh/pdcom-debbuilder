Format: 3.0 (quilt)
Source: pdcom
Binary: libpdcom-dev, libpdcom, libpdcom-dbg
Architecture: any
Version: 4.2.0.gd559770.13
Maintainer: Bjarne von Horn <vh@igh.de>
Homepage: http://www.etherlab.org
Standards-Version: 3.9.5
Build-Depends: debhelper (>= 9), libexpat1-dev, cmake, libsasl2-dev, libgnutls28-dev
Package-List:
 libpdcom4 deb libs optional arch=any
 libpdcom4-dbg deb debug extra arch=any
 libpdcom4-dev deb libdevel optional arch=any
Files:
 28d0b2d326515b75f7a3d4b1708e27bd 2217 pdcom4.debian.tar.gz
 0fb347664ecc7c4cddd949fb27bd807b 129754 pdcom4.orig.tar.gz
