Format: 3.0 (quilt)
Source: pdcom4
Binary: libpdcom4-dev, libpdcom4, libpdcom4-dbg
DEBTRANSFORM-RELEASE: 1
Architecture: any
Version: @VERSION@
Maintainer: Bjarne von Horn <vh@igh.de>
Homepage: http://www.etherlab.org
Standards-Version: 3.9.5
Build-Depends: debhelper (>= 9), libexpat1-dev, cmake, libsasl2-dev, libgnutls28-dev
Package-List:
 libpdcom4 deb libs optional arch=any
 libpdcom4-dbg deb debug extra arch=any
 libpdcom4-dev deb libdevel optional arch=any
Files:
